## Openbox round patch
My addition to [round patch](https://github.com/dylanaraps/openbox-patched), add round corners only for top on the window. Remove round of menu.

![screenshot](screenshot.png)

### How to patch on Debian\Devuan\Ubuntu and directives
```bash
$ mkdir openbox && cd openbox
$ apt source openbox
$ patch -p1 < openbox-3.6.2-rounded-corners.patch
$ debuild -i -us -uc -b
$ cd ..
$ sudo dpkg -i openbox_3.6.1-*_amd64.deb
```

### Todo's
- Disable round in maximize window
