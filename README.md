## Dotfiles
my current desktop configuration

![screenshot](/screenshots/all_in_one.png)

Obmenu-pp: [menu generator](https://github.com/VolkMilit/obmenu-pp)

Tiling controll: [Twily's tiling scripts](http://twily.info/scripts/tiling/)																																

Firefox: PaleMoon with [WhiteMoon](https://addons.palemoon.org/addon/white-moon/)

Icons: [Pop!](https://github.com/pop-os/icon-theme)

GTK Theme: [Slightly modifyed Arc](https://github.com/NicoHood/arc-theme)

WM Theme: [Modifyed Arc for OpenBox (trying to make System76 colors)](https://github.com/NicoHood/arc-theme)

Terminal: [My own fork of st called st++](https://gitlab.com/VolkMilit/st-pp)

Run: [dmenux](https://github.com/lvitals/dmenux)
